#include <iostream>
#include <string> 
#include <stdlib.h>     
#include <time.h> 

class worker
{
private:
	int age;
	std::string name;
	std::string work;
	std::string descripsion;
	int salary;
	bool happy;

public:
	worker(int age, std::string name, std::string work, std::string descripsion, int salary, bool happy)
	{
		this->age = age;
		this->descripsion = descripsion;
		this->happy = happy;
		this->name = name;
		this->salary = salary;
		this->work = work;
	}

	std::string getName()
	{
		return this->name;
	}

	std::string toString()
	{
		std::string isHappy = "false";
		if (this->happy)
			isHappy = "true";
		return "\n" + this->name + ":\nAge: " + std::to_string(this->age) + "\nWork: " + this->work + "\nSalary(in $) per month: " + std::to_string(this->salary) + "\nDescription: " + this->descripsion + "\nIs happy: " + isHappy + "\n";
	}
};

int main()
{
	worker workers[10] = {
		worker(23, "Arron", "Microsoft", "One of the major designers of the Microsoft Edge browser", 12000, true),
		worker(56, "Lily", "The best grocery!", "A seller in the local grocery store", 2000, false),
		worker(45, "Sofia", "Samsung", "Secretary in the Samsung main building", 5000, false),
		worker(27, "Hermione", "Xsigth", "A founder of the startup Xsight", 13000, true),
		worker(30, "Alon", "Goverment", "A trash cleaner of the goverment", 1200, true),
		worker(34, "Owen", "Apple", "Developer of IOS", 11000, false),
		worker(46, "Mika", "Hospital", "Brain surgeon", 23000, true),
		worker(54, "Nir", "Police", "Police Officer", 3500, true),
		worker(24, "Harry", "Tottenham Hotspuer", "An opening striker in the Lilywhites soocer team", 24000, true),
		worker(53, "Anton", "Motorola", "CEO of Motorola", 50000, false)
	};
	srand(time(0));
	int basePlace = 10 * rand() / (RAND_MAX + 1.0);
	basePlace = 10 * rand() / (RAND_MAX + 1.0);
	std::string allWorkersUsed = std::to_string(basePlace) + " ";
	int changingPlace = -1;
	int successful[3];
	int successfulOther[3];
	int howSuccessful;
	int previousRate;

	std::cout << "Welcome to my creative part of the assigment in bible that is 30% of my bagrot exam.\nThis game is showing you the relativity you havent noticed before.\n\nThe game:" << std::endl;
	for (int i = 0; i < 3; i++)
	{
		changingPlace = 10 * rand() / (RAND_MAX + 1.0);
		while(allWorkersUsed.find(std::to_string(changingPlace)) != std::string::npos )
			changingPlace = 10 * rand() / (RAND_MAX + 1.0);
		allWorkersUsed += std::to_string(changingPlace);
		
		std::cout << workers[changingPlace].toString() << "\n" << workers[basePlace].toString();
		
		std::cout << "\nHow successful " << workers[basePlace].getName() << " is? 1 - 10: ";
		std::cin >> successful[i];
		while (successful[i] > 10 || successful[i] < 1)
		{
			std::cerr << "Entered invalid value\nPlease try again: ";
			std::cin >> successful[i];
		}

		if (i == 1)
			previousRate = changingPlace;

		std::cout << "\nHow successful " << workers[changingPlace].getName() << " is? 1 - 10: ";
		std::cin >> successfulOther[i];
		while (successfulOther[i] > 10 || successfulOther[i] < 1)
		{
			std::cerr << "Entered invalid value\nPlease try again: ";
			std::cin >> successfulOther[i];
		}

	}

	if (successful[0] == successful[1] == successful[2])
		std::cout << "\nYou said that " << workers[basePlace].getName() << " is " << std::to_string(successful[0]) << " successful." << std::endl;
	else
		std::cout << "\nInteresting, you rated " << workers[basePlace].getName() << " differently, mabye its beacuse the people you compared him/her to?" << std::endl;

	howSuccessful = successfulOther[1];
	for (int i = 0; i < 3; i++)
	{
		changingPlace = 10 * rand() / (RAND_MAX + 1.0);
		while (allWorkersUsed.find(std::to_string(changingPlace)) != std::string::npos)
			changingPlace = 10 * rand() / (RAND_MAX + 1.0);
		allWorkersUsed += std::to_string(changingPlace);

		std::cout << workers[changingPlace].toString() << "\n" << workers[previousRate].toString();

		std::cout << "\nHow successful " << workers[previousRate].getName() << " is? 1 - 10: ";
		std::cin >> successful[i];
		while (successful[i] > 10 || successful[i] < 1)
		{
			std::cerr << "Entered invalid value\nPlease try again: ";
			std::cin >> successful[i];
		}

		std::cout << "\nHow successful " << workers[changingPlace].getName() << " is? 1 - 10: ";
		std::cin >> successfulOther[i];
		while (successfulOther[i] > 10 || successfulOther[i] < 1)
		{
			std::cerr << "Entered invalid value\nPlease try again: ";
			std::cin >> successfulOther[i];
		}
	}

	if (successful[0] == successful[1] == successful[2] == howSuccessful)
		std::cout << "\nYou said that " << workers[previousRate].getName() << " is " << std::to_string(successful[0]) << " successful, you rated the same in the first game and in the second!" << std::endl;
	else
		std::cout << "\nInteresting, you rated " << workers[previousRate].getName() << " differently, mabye its beacuse the people you comperd him/her to? you rated differently in the first game and the second." << std::endl;




	return 0;
}